import * as firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

const apiKey = process.env.REACT_APP_API_KEY;
const firebaseConfig = {
  apiKey: apiKey,
  authDomain: "learn-the-words-3cda4.firebaseapp.com",
  databaseURL: "https://learn-the-words-3cda4.firebaseio.com",
  projectId: "learn-the-words-3cda4",
  storageBucket: "learn-the-words-3cda4.appspot.com",
  messagingSenderId: "354148926128",
  appId: "1:354148926128:web:a9383ffb9450dd7841b175",
};

class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig);

    this.auth = firebase.auth();
    this.database = firebase.database();
    this.userUid = null;
  }

  setUserUid = (uid) => {
    this.userUid = uid;
    console.log("###:this.userUid:", this.userUid);
  };

  signWithEmail = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  createUserEmail = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  getUserCardsRef = () => this.database.ref(`/cards/${this.userUid}`);
}

export default Firebase;
