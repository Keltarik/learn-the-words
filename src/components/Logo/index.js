import React from "react";
import cl from "classnames";
import s from "./Logo.module.scss";
import { ReactComponent as ReactLogo } from "../../logo.svg";

const Logo = ({logoText = "English 'n React", style}) => {
  return (
    <div className={cl(s.logoBlock, style )}>
      <ReactLogo />
      <div className={s.logo_text}>{logoText}</div>
    </div>
  );
};

export default Logo;
