import React, { Component } from "react";
import s from "./HeaderTop.module.scss";
import Logo from "../Logo/index";
import { Tooltip } from "antd";
import { UserOutlined, LogoutOutlined } from "@ant-design/icons";
import FirebaseContext from "../../context/firebaseContext";

class HeaderTop extends Component {
  logOut = () => {
    const { history } = this.props;
    this.context.auth.signOut();
    history.push("/login");
  };

  render() {
    return (
      <header className={s.pattern}>
        <div className={s.container}>
          <div className={s.header}>
            <Logo />
            <div className={s.menuBlock}>
              <ul className={s.menuLinks}>
                {/* <li>
                <a href="#">{userName}</a>
             </li> */}
                <li>
                  <div className="button button-sm">
                    <UserOutlined
                      style={{ fontSize: 14, paddingRight: 10, paddingLeft: 5 }}
                    />{" "}
                    {this.props.userName}
                  </div>
                  <Tooltip placement="bottom" title="Выйти из аккаунта">
                    <div className={s.logout} onClick={this.logOut}>
                      <LogoutOutlined />
                    </div>
                  </Tooltip>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

HeaderTop.contextType = FirebaseContext;

export default HeaderTop;
