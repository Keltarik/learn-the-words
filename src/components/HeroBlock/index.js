import React from "react";
import s from "./HeroBlock.module.scss";
import { Button } from "antd";
import reactImg from "./img/react.png";
import reduxImg from "./img/redux.png";
import sassImg from "./img/sass.png";
import firebaseImg from "./img/firebase.png";

const HeroBlock = ({ title, subTitle, buttonText }) => {
  return (
    <div className={s.container}>
      <div>
        <h1 className={s.heroTitle}>{title}</h1>
        <p className={s.heroSubtitle}>{subTitle}</p>
        <Button type="primary" size="large">
          <a href="#words">{buttonText}</a>
        </Button>
        <div className={s.heroMedia}>
          <div className={s.featureBlock}>
            <div className="feature-block-icon mb-4 text-primary">
              <div>
                <ion-icon name="happy-outline"></ion-icon>
              </div>
            </div>
            <h3 className="h4 mb-2 translate-text translate-up is-animated">
              <span>Весело</span>
            </h3>
            <p
              className="translate-text translate-up is-animated"
              data-translate-delay="150"
            >
              <span>
                Изучайте английский в веселой игровой форме.
              </span>
            </p>
          </div>

          <div className={s.featureBlock}>
            <div className="feature-block-icon">
                 <ion-icon name="speedometer-outline"></ion-icon>
            </div>
            <h3 className="h4">
              <span>Эффективно</span>
            </h3>
            <p>
                Научно доказано, что изучение слов в карточном формате помогает быстрее освоить язык.
            </p>
          </div>   

          <div className={s.featureBlock}>
            <div className="feature-block-icon">
            <ion-icon name="desktop-outline"></ion-icon>
            </div>
            <h3 className="h4">
              <span>Современно</span>
            </h3>
            <p>
                Забудьте про бумажные словари. Пора использовать современные методы обучения.
            </p>
          </div>   

          <div className={s.featureBlock}>
            <div className="feature-block-icon">
            <ion-icon name="color-palette-outline"></ion-icon>
            </div>
            <h3 className="h4">
              <span>Удобно</span>
            </h3>
            <p>
                Наш интерфейс спроектирован для максимального удобства пользователя.
            </p>
          </div>   

        </div>
        <div className={s.madeWithText}>Made With:</div>
        <ul className={s.madeWithBlock}>
          <li>
            <img src={reactImg} alt="react" />
          </li>
          <li>
            <img src={reduxImg} alt="redux" />
          </li>
          <li>
            <img src={sassImg} alt="sass" />
          </li>
          <li>
            <img src={firebaseImg} alt="firebase" />
          </li>
        </ul>
      </div>
    </div>
  );
};

export default HeroBlock;
