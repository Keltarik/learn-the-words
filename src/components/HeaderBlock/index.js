import React from "react";
import s from "./HeaderBlock.module.scss";

const HeaderBlock = ({ children, anchor }) => {
  return (
    <section id={anchor} className={s.cover}>
      <div className={s.wrap}>{children}</div>
    </section>
  );
};
export default HeaderBlock;
