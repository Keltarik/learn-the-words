import React, { Component } from "react";
import cl from "classnames";
import s from "./Card.module.scss";
import { Tooltip } from "antd";
import {
  PauseCircleOutlined,
  CheckSquareOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { fileToObject } from "antd/lib/upload/utils";

class Card extends React.Component {
  state = {
    done: false,
    isRemembered: false,
  };

  handleCardClick = () => {
    if (this.state.isRemembered) {
      this.setState(({ done }) => ({
        done: true,
      }));
    } else {
      this.setState(({ done }) => ({
        done: !done,
      }));
    }
  };

  handleIsRememberClick = () => {
    if (this.state.isRemembered) {
      this.setState(({ done, isRemembered }) => ({
        done: false,
        isRemembered: false,
      }));
    } else if (this.state.done) {
      this.setState(({ done, isRemembered }) => ({
        done: true,
        isRemembered: true,
      }));
    } else {
      this.handleCardClick();
      this.setState(({ isRemembered }, props) => ({
        isRemembered: !isRemembered,
      }));
    }
  };

  handleDeletedClick = () => {
    this.props.onDeleted();
  };

  render() {
    const { eng, rus, img } = this.props;
    const { done, isRemembered, toRemember } = this.state;

    return (
      <div className={s.root}>
        <div
          className={cl(s.card, {
            [s.done]: done,
            [s.isRemembered]: isRemembered,
            [s.toRemember]: toRemember === true,
          })}
          onClick={this.handleCardClick}
        >
          <div className={s.cardInner}>
            <div className={s.cardFront}>{eng}</div>
            <div className={s.cardBack}>{rus}</div>
          </div>
        </div>
        <Tooltip title="Пометить слово как запомненное">
          <div
            className={cl(s.icons, {
              [s.isRemembered]: isRemembered,
            })}
          >
            <CheckSquareOutlined
              title=""
              onClick={this.handleIsRememberClick}
            />
          </div>
        </Tooltip>

        <Tooltip title="Удалить слово из словаря">
          <div className={s.icons}>
            <DeleteOutlined title="Удалить" onClick={this.handleDeletedClick} />
          </div>
        </Tooltip>
      </div>
    );
  }
}

export default Card;
