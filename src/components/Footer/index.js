import React from 'react';
import s from './Footer.module.scss';

const Footer = ({children}) => {
	return <footer> <div className={s.footer_text}>{children}</div> </footer>
}

export default Footer;