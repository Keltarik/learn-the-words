import React, { Component } from "react";
import Card from "../Card";
import FirebaseContext from "../../context/firebaseContext";
import cl from "classnames";
import s from "./CardList.module.scss";
import getTranslateWord from "../../services/yandex-dictionary";
import { Button, Tooltip, Input } from "antd";
const { Search } = Input;

class CardList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      transWord: "",
      origWord: "",
      incorrect: false,
      isBusy: false,
      tooltipTitle: "",
    };
  }

  inputChangeHandler = (e) => {
    this.setState({
      origWord: e.target.value.toLowerCase(),
    });
  };

  clearInputs = () => {
    this.setState(() => {
      return {
        origWord: "",
        transWord: "",
        isBusy: false,
      };
    });
  };

  checkIfStringCyrillic = (str) => {
    const russian = /^[а-яё -]+$/;
    return russian.test(str);
  };

  submitWordError = (text) => {
    this.setState({
      incorrect: true,
      isBusy: false,
      origWord: "",
      transWord: "",
      tooltipTitle: text,
    });
    let timeOut = setTimeout(() => {
      this.setState({
        incorrect: false,
      });
    }, 2000);
  };

  checkForDuplicates = async (word, lang) => {
    return this.context.database
      .ref("/cards/" + this.context.userUid)
      .orderByChild(lang)
      .equalTo(word)
      .once("value")
      .then((snapshot) => {
        if (snapshot.exists()) {
          const userData = snapshot.val();
          console.log("exists!", userData);
          return false;
        } else {
          return true;
        }
      });
  };

  getWord = async () => {
    const { transWord, origWord } = this.state;
    let getWord;
    try {
      if (this.checkIfStringCyrillic(origWord)) {
        const lang = "eng";
        getWord = await getTranslateWord(this.state.origWord);
        const translateWord = getWord[0].tr[0].text;
        this.checkForDuplicates(translateWord, lang).then((res) => {
          if (res == true) {
            this.props.onAddItem(translateWord, origWord);
          } else {
            this.submitWordError("Такое слово уже добавлено");
          }
        });
      } else {
        const lang = "rus";
        getWord = await getTranslateWord(this.state.origWord, "en-ru");
        const translateWord = getWord[0].tr[0].text;
        this.checkForDuplicates(translateWord, lang).then((res) => {
          if (res == true) {
            this.props.onAddItem(origWord, translateWord);
          } else {
            this.submitWordError("Такое слово уже добавлено");
          }
        });
      }
      this.clearInputs();
    } catch {
      this.submitWordError("Мы не нашли такого слова");
    }
  };

  submitFormHandler = async (e) => {
    e.preventDefault();
    this.setState(
      {
        isBusy: true,
      },
      this.getWord
    );
  };

  render() {
    const { item = [], onDeletedItem, onSubmitForm } = this.props;
    const { isBusy, incorrect, tooltipTitle } = this.state;
    return (
      <>
        <form className={s.form} onSubmit={this.submitFormHandler}>
          <Tooltip visible={incorrect} title={tooltipTitle}>
            <Search
              size="large"
              value={this.state.origWord}
              onChange={this.inputChangeHandler}
              type="text"
              placeholder="Напишите слово по русски или английски"
              className={cl(s.addCardInput, { [s.inactive]: incorrect })}
              loading={isBusy}
              required
            />
          </Tooltip>
          <Button
            size="large"
            background="#fff"
            type="primary"
            htmlType="submit">
            Добавить новое слово
          </Button>
        </form>
        <div className={s.root}>
          {item.map(({ eng, rus, id }) => (
            <Card
              onDeleted={() => {
                onDeletedItem(id);
              }}
              handleSubmitForm={() => {
                onSubmitForm(eng, rus);
              }}
              key={id}
              eng={eng}
              rus={rus}
            />
          ))}
        </div>
      </>
    );
  }
}
CardList.contextType = FirebaseContext;
export default CardList;
