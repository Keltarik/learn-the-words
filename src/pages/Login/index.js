import React, { Component } from "react";
import { Layout, Form, Input, Button, Tabs } from "antd";
import s from "./Login.module.scss";
import FirebaseContext from "../../context/firebaseContext";
import Logo from "../../components/Logo";

const { TabPane } = Tabs;
const { Content } = Layout;

class LoginPage extends Component {
  state = {
    validation: "",
  };

  onSignIn = ({ email, password }) => {
    const { signWithEmail } = this.context;
    const { history } = this.props;

    signWithEmail(email, password)
      .then((res) => {
        console.log("###: res", res);
        history.push("/");
      })
      .catch((error) => {
        this.setState({ validation: error.message });
        setTimeout(() => {
          this.setState({ validation: "" });
        }, 4000);
      });
  };

  onSignUp = ({ email, password }) => {
    this.context
      .createUserEmail(email, password)
      .then((res) => {
        console.log("###: createUser res", res);
      })
      .catch((error) => {
        this.setState({ validation: error.message });
        setTimeout(() => {
          this.setState({ validation: "" });
        }, 4000);
      });
  };

  onFinishFailed = (errorMsg) => {
    console.log("###:errorMsg", errorMsg);
    //this.setState({ validation: errorMsg });
  };

  renderFormSignIn = () => {
    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };
    const tailLayout = {
      wrapperCol: { offset: 8, span: 16 },
    };
    return (
      <div className={s.form_inner}>
        <div className={s.failed}>{this.state.validation}</div>
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={this.onSignIn}
          onFinishFailed={this.onFinishFailed}>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: "Please input your email!" }]}>
            <Input />
          </Form.Item>

          <Form.Item
            label="Пароль"
            name="password"
            minLength="8"
            rules={[
              { required: true, message: "Please input your password!" },
            ]}>
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Войти
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  };

  renderFormSignUp = () => {
    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };
    const tailLayout = {
      wrapperCol: { offset: 8, span: 16 },
    };
    return (
      <div className={s.form_inner}>
        <div className={s.failed}>{this.state.validation}</div>
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={this.onSignUp}
          onFinishFailed={this.onFinishFailed}>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: "Please input your email!" }]}>
            <Input />
          </Form.Item>

          <Form.Item
            label="Пароль"
            name="password"
            minLength="8"
            rules={[
              { required: true, message: "Please input your password!" },
            ]}>
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Зарегистрироваться
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  };

  render() {
    return (
      <Layout>
        <Content>
          <div className={s.root}>
            <div className={s.form_wrap}>
              <Logo style="login_logoBlock" />
              <Tabs defaultActiveKey="1">
                <TabPane tab={<span>Авторизоваться</span>} key="1">
                  {this.renderFormSignIn()}
                </TabPane>
                <TabPane tab={<span>Зарегистрироваться</span>} key="2">
                  {this.renderFormSignUp()}
                </TabPane>
              </Tabs>
              {/* {this.state.signIn
                ? this.renderFormSignIn()
                : this.renderFormSignUp()} */}
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}
LoginPage.contextType = FirebaseContext;

export default LoginPage;
