import React, { Component } from "react";
import nanoid from "nanoid";
import HeroBlock from "../../components/HeroBlock";
import CardList from "../../components/CardList";
import HeaderTop from "../../components/HeaderTop";
import Header from "../../components/Header";
import HeaderBlock from "../../components/HeaderBlock/";
import Paragraph from "../../components/Paragraph";
import Footer from "../../components/Footer";
import FirebaseContext from "../../context/firebaseContext";

class HomePage extends Component {
  state = {
    wordArr: [],
  };

  componentDidMount() {
    this.context.getUserCardsRef().on("value", (res) => {
      if (res.val() === null) {
        this.setState({ wordArr: [] });
      } else {
        this.setState({ wordArr: Object.values(res.val()) || [] }, () => {});
      }
    });
  }

  handleDeletedItem = (id) => {
    const { wordArr } = this.state;
    const newWordArr = wordArr.filter((item) => item.id !== id);
    this.context.getUserCardsRef().set(newWordArr);
  };

  addItemHandler = (eng, rus) => {
    const { wordArr } = this.state;
    const newArr = [
      ...wordArr,
      {
        eng,
        rus,
        id: nanoid(),
      },
    ];
    this.context.getUserCardsRef().set(newArr);
  };

  render() {
    const { wordArr } = this.state;
    console.log("####user:", this.context);
    return (
      <>
        <HeaderTop
          userName={
            this.props.user !== undefined ? this.props.user.email : "user"
          }>
          English 'n React
        </HeaderTop>
        <section className="hero">
          <HeroBlock
            title="Учим английский сами!"
            subTitle="С помощью нашего Single Page приложения Вы сможете выучить слова на английском, составить собственный словарь и отслеживать прогресс."
            buttonText="Начни сейчас!"
          />
        </section>
        {/* <HeaderBlock>
          <Header>Время учить слова онлайн</Header>
          <Paragraph>
            Используйте карточки для запоминания и пополняйте активный словарный
            запас
          </Paragraph>
        </HeaderBlock> */}
        <HeaderBlock anchor="words">
          <Header>Слова для запоминания</Header>
          <Paragraph>
            Добавьте Ваши собственные слова в список. Английский или русский
            перевод добавится автоматически, благодаря сервису "Яндекс
            Переводчик".{" "}
          </Paragraph>
          <CardList
            onAddItem={this.addItemHandler}
            onDeletedItem={this.handleDeletedItem}
            item={wordArr}
          />
        </HeaderBlock>
        <Footer>Created by Dmitry Mordvintsev</Footer>
      </>
    );
  }
}
HomePage.contextType = FirebaseContext;

export default HomePage;
