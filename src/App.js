import React, { Component } from "react";
import s from "./App.module.scss";
import { Spin, Layout, Menu } from "antd";
import LoginPage from "./pages/Login";
import HomePage from "./pages/Home";
import FirebaseContext from "./context/firebaseContext";
import { BrowserRouter, Route, Link } from "react-router-dom";

const { Header, Content } = Layout;

class App extends Component {
  state = {
    user: null,
  };

  componentDidMount() {
    console.log("####: context", this.context);
    const { auth, setUserUid, userUid } = this.context;

    auth.onAuthStateChanged((user) => {
      if (user) {
        setUserUid(user.uid);
        this.setState({
          user: user,
        });
      } else {
        setUserUid(null);
        this.setState({
          user: false,
        });
      }
    });
  }

  render() {
    const { user } = this.state;

    if (user === null) {
      return (
        <div className={s.loader_wrap}>
          <Spin size="large" />
        </div>
      );
    }

    // return <>{user ? <HomePage user={user} /> : <LoginPage />}</>;
    return (
      <BrowserRouter>
        <Route path="/login" component={LoginPage} />
        <Route
          render={(props) => {
            console.log("####:props", props);
            const {
              history: { push },
            } = props;
            return (
              <>
                <Header>
                  <Menu theme="dark" mode="horizontal">
                    <Menu.Item key="1">
                      <Link to="/">Home</Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                      <Link to="/about">About</Link>
                    </Menu.Item>
                    <Menu.Item key="3" onClick={() => push("/contact")}>
                      Contact
                    </Menu.Item>
                  </Menu>
                </Header>
                <Content>
                  <Route path="/" exact component={HomePage} />
                  <Route path="/home" component={HomePage} />
                  <Route path="/about" render={() => <h1>Немного о себе</h1>} />
                  <Route
                    path="/contact"
                    render={() => <h1>Немного контактов</h1>}
                  />
                </Content>
              </>
            );
          }}
        />
      </BrowserRouter>
    );
  }
}
App.contextType = FirebaseContext;

export default App;
